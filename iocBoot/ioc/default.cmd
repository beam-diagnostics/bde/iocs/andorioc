###############################################################################
#
# PBI system   : Generic Andor CCD camera
# Location     : lab
#
# Support      : https://jira.esss.lu.se/projects/PBITECH
# Wiki         : https://confluence.esss.lu.se/display/PBITECH
#
###############################################################################

# location of the system (section-subsection)
# lab example
epicsEnvSet("RACKROW",                      "PBILAB")
# acquisition unit logical name
epicsEnvSet("ACQ_UNIT",                     "GENERIC")
# acquisition device ID or name
epicsEnvSet("ACQ_DEVID",                    "GENERIC")
# acquisition device serial number (0 means any)
epicsEnvSet("ACQ_SERNO",                    "0")
# acquisition device detector geometry
epicsEnvSet("ACQ_XSIZE",                    "2048")
epicsEnvSet("ACQ_YSIZE",                    "2048")

# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")

# include the main startup file
cd startup
< main.cmd
