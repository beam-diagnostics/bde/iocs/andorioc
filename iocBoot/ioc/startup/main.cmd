###############################################################################
# IOC main startup file
###############################################################################

# get paths to EPICS base and modules
< envPaths

errlogInit(20000)

dbLoadDatabase("$(IOCAPP)/dbd/iocApp.dbd")
iocApp_registerRecordDeviceDriver(pdbbase)

# PATH: we need caput and caRepeater
epicsEnvSet("PATH",                         "$(PATH):$(EPICS_BASE)/bin/linux-x86_64")
# EPICS_DB_INCLUDE_PATH: list all module db folders
epicsEnvSet("EPICS_DB_INCLUDE_PATH",        "$(TOP)/db:$(ASYN)/db:$(AUTOSAVE)/db:$(ADCORE)/db:$(ADANDOR)/db:$(ADMISC)/db:$(ANDORAPP)/db")

###############################################################################
# setup before iocBoot()
###############################################################################

# acquisition (SIS8300) setup
< acq.setup

# autosave setup
< autosave.setup



iocInit()



###############################################################################
# initialization after iocBoot()
###############################################################################

# comment out if autosave is not used
< autosave.init

# comment out if acquisition is not used
< acq.init

###############################################################################
# IOC running!
###############################################################################
