###############################################################################
#
# PBI system   : iDus Andor CCD camera
# Location     : lab
#
# Support      : https://jira.esss.lu.se/projects/PBITECH
# Wiki         : https://confluence.esss.lu.se/display/PBITECH
#
###############################################################################

# location of the system (section-subsection)
# lab example
epicsEnvSet("RACKROW",                      "PBILAB")
# acquisition unit logical name
epicsEnvSet("ACQ_UNIT",                     "IDUS1")
# acquisition device ID or name
epicsEnvSet("ACQ_DEVID",                    "IDUS1")
# acquisition device serial number (0 means any)
epicsEnvSet("ACQ_SERNO",                    "21055")
# acquisition device detector geometry
epicsEnvSet("ACQ_XSIZE",                    "1024")
epicsEnvSet("ACQ_YSIZE",                    "255")

# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")

# include the main startup file
cd startup
< main.cmd
